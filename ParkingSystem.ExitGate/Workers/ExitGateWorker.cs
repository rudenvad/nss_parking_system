using MassTransit;
using ParkingSystem.Common.Devices;
using ParkingSystem.Common.Opperations;

namespace ParkingSystem.ExitGate.Workers
{
    public class ExitGateWorker : BackgroundService
    {
        private readonly IBus _bus;

        private readonly IDisplay _display;
        private readonly IBarrier _barrier;

        private TaskCompletionSource<TicketScannedEventArgs> TicketScannedWaiter { get; set; }

        public ExitGateWorker(IBus bus, IDisplay display, IScanner scanner, IBarrier barrier)
        {
            _bus = bus; 

            _display = display;
            _barrier = barrier;

            scanner.TicketScanned += (s, e) =>
            {
                TicketScannedWaiter?.SetResult(e);
            };
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try { await RunFlow(); }
                catch { }
            }
        }

        private async Task RunFlow()
        {
            await _display.Show("Scan your ticket");

            TicketScannedWaiter = new TaskCompletionSource<TicketScannedEventArgs>();
            var ticketScannedArgs = await TicketScannedWaiter.Task;
            TicketScannedWaiter = null;

            if (ticketScannedArgs.Error != null)
            {
                await _display.Show(ticketScannedArgs.Error);
                await Task.Delay(3000);
                return;
            }

            await _display.Show("Requesting permission to exit...");
            var exitPermissionResponse = await _bus.Request<ExitPermission.Request, ExitPermission.Response>(new ExitPermission.Request
            {
                TicketId = ticketScannedArgs.TicketId
            });

            if (!exitPermissionResponse.Message.Permitted)
            {
                await _display.Show(exitPermissionResponse.Message.Error);
                await Task.Delay(3000);
                return;
            }

            await _display.Show("Exit permitted, have a nice day!");

            await _barrier.Open();
            await Task.Delay(5000);
            await _barrier.Close();
        }
    }
}