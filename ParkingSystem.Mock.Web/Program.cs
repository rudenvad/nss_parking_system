using ParkingSystem.Common.Settings;
using ParkingSystem.Mock.Web.Hubs;
using Serilog;
using Serilog.Sinks.Elasticsearch;

var builder = WebApplication.CreateBuilder(args);

var xSection = builder.Configuration.GetSection("X");
var elasticsearchSettings = xSection.GetSection("Elasticsearch").Get<ElasticsearchSettings>();

Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .Enrich.WithMachineName()
    .WriteTo.Debug()
    .WriteTo.Console()
    .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticsearchSettings.Uri))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"logs-{nameof(ParkingSystem.Mock).ToLower()}"
    })
    .ReadFrom.Configuration(new ConfigurationBuilder()
        .AddJsonFile("appsettings.json")
        .Build())
    .CreateLogger();

builder.Host.UseSerilog();

builder.Services.AddSignalR();

var app = builder.Build();

app.MapHub<MockHub>("/hub");
app.UseFileServer();

app.Run();
