import { BarrierState } from "./BarrierState.js"
import { EntranceGate } from "./EntranceGate.js"
import { ExitGate } from "./ExitGate.js"
import { PaymentMachine } from "./PaymentMachine.js"
import { DeviceId } from "./DeviceId.js"
import { Topic } from "./Topic.js"

class Main {
    static #hubURL = `/hub`;

    constructor(args)
    {
        this.entranceGate = new EntranceGate();
        this.exitGate = new ExitGate(); 
        this.paymentMachine = new PaymentMachine();

        this.hubConnection;

        //#region Hub Connection Init   
        this.BuildHubConnection();
        this.SetConnectionOn();
        //#endregion

        //# Send message 'Broadcast' with topic 'Clicked' & payload == null
        this.entranceGate.buttonEl.addEventListener(`click`, e =>
        {
            this.SendMessage(DeviceId.EntranceButton, Topic.Clicked, null);
        });

        //# Send message 'Broadcast' with topic 'Clicked' & payload
        this.exitGate.buttonEl.addEventListener(`click`, e =>
        {
            const payload = this.exitGate.ScannerScan();
            this.SendMessage(DeviceId.ExitScanner, Topic.TicketScanned, payload);
        });

        this.paymentMachine.scannerButtonEl.addEventListener(`click`, e =>
        {
            const payload = this.paymentMachine.ScannerScan();
            this.SendMessage(DeviceId.PaymentScanner, Topic.TicketScanned, payload);
        });

        this.paymentMachine.terminalButtonCancelEl.addEventListener(`click`, e =>
        {
            this.SendMessage(DeviceId.PaymentTerminal, Topic.PaymentResult, `false`);
        });

        this.paymentMachine.terminalButtonPayEl.addEventListener(`click`, e =>
        {
            this.SendMessage(DeviceId.PaymentTerminal, Topic.PaymentResult, `true`);
        });

        //#region Start Hub Connection
        this.StartHubConnection();
        //#endregion
    }

    async BuildHubConnection()
    {
        this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl(Main.#hubURL)
            .build();
    }

    async SetConnectionOn()
    {
        this.hubConnection.on(Topic.Broadcast, (deviceId, topic, payload) =>
        {
            console.log(`Broadcast; deviceId: ${deviceId}, topic: ${topic}, payload: ${payload}`);

            switch (deviceId)
            {
                //#region Entrance devices
                case DeviceId.EntranceDisplay:
                    switch (topic)
                    {
                        case Topic.Show: this.OnEntranceShow(payload); break;
                        default: break;
                    }
                    break;
                case DeviceId.EntranceBarrier:
                    switch (topic)
                    {
                        case Topic.Open: this.OnEntranceOpen(); break;
                        case Topic.Close: this.OnEntranceClose(); break;
                        default: break;
                    }
                    break;
                case DeviceId.EntranceCamera:
                    switch (topic)
                    {
                        case Topic.RecognizeLicencePlate: this.OnEntranceRecognizeLicencePlate(DeviceId.EntranceCamera); break;
                        default: break;
                    }
                    break;
                case DeviceId.EntrancePrinter:
                    switch (topic)
                    {
                        case Topic.Print: this.OnEntrancePrint(payload); break;
                        default: break;
                    }
                    break;
                //#endregion

                //#region Exit devices
                case DeviceId.ExitDisplay:
                    switch (topic)
                    {
                        case Topic.Show: this.OnExitShow(payload); break;
                        default: break;
                    }
                case DeviceId.ExitBarrier:
                    switch (topic)
                    {
                        case Topic.Open: this.OnExitOpen(); break;
                        case Topic.Close: this.OnExitClose(); break;
                        default: break;
                    }
                    break;
                //#endregion

                //#region Payment devices
                case DeviceId.PaymentDisplay:
                    switch (topic)
                    {
                        case Topic.Show: this.OnPaymentShow(payload); break;
                        default: break;
                    }
                //#endregion
                default: break;
            }

        });

        this.hubConnection.onclose(async () => { await this.StartHubConnection(); });
    }

    async StartHubConnection()
    {
        await this.hubConnection.start()
            .then(() => console.log("SignalR Connected."))
            .catch((e) =>
            {
                console.log(e);
                setTimeout(this.StartHubConnection, 5000);
            });
    }

    async SendMessage(deviceId, topic, payload)
    {
        console.log(`SendMessage: Broadcast; deviceId: ${deviceId}, topic: ${topic}, payload: ${payload}`);
        await this.hubConnection.send(Topic.Broadcast, deviceId, topic, payload)
            .catch(e => console.error(e));
    }

    //#region Entrance handlers
    async OnEntranceShow(content)
    {
        //XSS payload

        this.entranceGate.DisplaySetContent(content);
    }

    async OnEntranceOpen()
    {
        this.entranceGate.BarrierSetState(BarrierState.Opened);
    }

    async OnEntranceClose()
    {
        this.entranceGate.BarrierSetState(BarrierState.Closed);
    }

    async OnEntranceRecognizeLicencePlate(cameraName)
    {
        this.SendMessage(cameraName, Topic.LicencePlateResult, this.entranceGate.CameraRecognizeLicencePlate());
    }

    async OnEntrancePrint(ticket)
    {
        //XSS payload

        this.entranceGate.PrinterPrint(ticket);
    }
    //#endregion

    //#region Exit handlers
    async OnExitShow(content)
    {
        //XSS payload

        this.exitGate.DisplaySetContent(content);
    }

    async OnExitOpen()
    {
        this.exitGate.BarrierSetState(BarrierState.Opened);
    }

    async OnExitClose()
    {
        this.exitGate.BarrierSetState(BarrierState.Closed);
    }
    //#endregion

    //#region Exit handlers
    async OnPaymentShow(content)
    {
        this.paymentMachine.DisplaySetContent(content);
    }
    //#endregion
}

const main = new Main();
