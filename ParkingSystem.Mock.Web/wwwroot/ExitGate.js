﻿import { BarrierState } from "./BarrierState.js";

class ExitGate
{
    constructor()
    {
        const prefixId = `#exit-gate-`;

        this.displayEl = document.querySelector(`${prefixId}display`);
        this.barrierEl = document.querySelector(`${prefixId}barrier`);
        this.scanerEl = document.querySelector(`${prefixId}scanner`);
        this.buttonEl = document.querySelector(`${prefixId}button`);

        this.displayEl.classList.add(...[`bg-light`]);
        this.scanerEl.classList.add(...[`bg-light`]);
        this.barrierEl.classList.add(`text-white`);

        this.buttonEl.disabled = true;
        this.scanerEl.onchange = () =>
        {
            if (this.scanerEl.value)
            {
                this.buttonEl.disabled = false;
            }
            else
            {
                this.buttonEl.disabled = true;
            }
        };

        this.DisplaySetContent(`Scan ticket`);

        this.BarrierSetState(BarrierState.Closed);
        this.ScannerScan(`no-ticket`);
    }

    DisplaySetContent(content) { this.displayEl.innerHTML = content; }
    BarrierSetState(state)
    {
        const dangerClass = `bg-danger`;
        const successClass = `bg-success`;

        if (state === BarrierState.Opened)
        {
            this.barrierEl.classList.remove(dangerClass);
            this.barrierEl.classList.add(successClass);
        }
        else if (state === BarrierState.Closed)
        {
            this.barrierEl.classList.remove(successClass);
            this.barrierEl.classList.add(dangerClass);
        }

        this.barrierEl.innerHTML = state;
    }

    ScannerScan()
    {
        const tmp = this.scanerEl.value;
        this.scanerEl.value = ``;
        return tmp;
    }
    GetBarrierState() { return this.barrierEl.innerHTML; }
}


export { ExitGate }