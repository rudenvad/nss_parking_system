﻿class DeviceId
{
    static EntranceButton = `EntranceGate:ButtonMock`;
    static EntranceDisplay = `EntranceGate:DisplayMock`;
    static EntrancePrinter = `EntranceGate:PrinterMock`;
    static EntranceBarrier = `EntranceGate:BarrierMock`;
    static EntranceCamera = `EntranceGate:CameraMock`;

    static ExitDisplay = `ExitGate:DisplayMock`;
    static ExitScanner = `ExitGate:ScannerMock`;
    static ExitBarrier = `ExitGate:BarrierMock`;

    static PaymentDisplay = `PaymentMachine:DisplayMock`;
    static PaymentScanner = `PaymentMachine:ScannerMock`;
    static PaymentTerminal = `PaymentMachine:TerminalMock`;
}
export { DeviceId }