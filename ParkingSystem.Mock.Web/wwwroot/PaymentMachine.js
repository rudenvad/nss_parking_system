﻿class PaymentMachine
{
    constructor()
    {
        const prefixId = `#payment-machine-`;

        this.displayEl = document.querySelector(`${prefixId}display`);
        this.scanerEl = document.querySelector(`${prefixId}scanner`);
        this.terminalEl = document.querySelector(`${prefixId}terminal`)
        this.terminalButtonCancelEl = document.querySelector(`${prefixId}button-terminal-cancel`);
        this.terminalButtonPayEl = document.querySelector(`${prefixId}button-terminal-pay`);
        this.scannerButtonEl = document.querySelector(`${prefixId}button-scanner`);

        this.displayEl.classList.add(...[`bg-light`]);
        this.scanerEl.classList.add(...[`bg-light`]);

        this.scannerButtonEl.disabled = true;
        this.scanerEl.onchange = () =>
        {
            if (this.scanerEl.value)
            {
                this.scannerButtonEl.disabled = false;
            }
            else
            {
                this.scannerButtonEl.disabled = true;
            }
        };

        this.DisplaySetContent(`Scan ticket`);
    }

    DisplaySetContent(content) { this.displayEl.innerHTML = content; }
    ScannerScan()
    {
        const tmp = this.scanerEl.value;
        this.scanerEl.value = ``;
        return tmp;
    }
}


export { PaymentMachine }