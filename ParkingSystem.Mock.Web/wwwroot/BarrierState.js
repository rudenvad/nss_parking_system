class BarrierState
{
    constructor(){}
    
    static Opened = `Opened`;
    static Closed = `Closed`;
}

export { BarrierState }