import { BarrierState } from "./BarrierState.js";

class EntranceGate
{
    constructor()
    {
        const prefixId = `#entrance-gate-`;

        this.displayEl = document.querySelector(`${prefixId}display`);
        this.barrierEl = document.querySelector(`${prefixId}barrier`);
        this.cameraEl = document.querySelector(`${prefixId}camera`);
        this.printerEl = document.querySelector(`${prefixId}printer`);
        this.buttonEl = document.querySelector(`${prefixId}button`);

        this.displayEl.classList.add(...[`bg-light`]);
        this.cameraEl.classList.add(...[`bg-light`]);
        this.printerEl.classList.add(...[`bg-light`]);
        this.barrierEl.classList.add(`text-white`);

        this.cameraEl.style.resize = `none`;
        this.cameraEl.setAttribute(`placeholder`, `Licence plate to read`);

        this.printerEl.style.resize = `none`;
        this.printerEl.setAttribute(`placeholder`, `no-ticket`);
        this.printerEl.readOnly = true;

        this.DisplaySetContent(`Click button to get ticket`);

        this.BarrierSetState(BarrierState.Closed);
    }

    DisplaySetContent(content) { this.displayEl.innerHTML = content; }

    CameraRecognizeLicencePlate()
    {
        const tmp = this.cameraEl.value;
        this.cameraEl.value = ``;
        return tmp;
    }

    BarrierSetState(state)
    {
        const dangerClass = `bg-danger`;
        const successClass = `bg-success`;

        if (state === BarrierState.Opened)
        {
            this.barrierEl.classList.remove(dangerClass);
            this.barrierEl.classList.add(successClass);
        }
        else if (state === BarrierState.Closed)
        {
            this.barrierEl.classList.remove(successClass);
            this.barrierEl.classList.add(dangerClass);
        }

        this.barrierEl.innerHTML = state;
    }
    PrinterPrint(ticket) { this.printerEl.value = ticket; }

    GetBarrierState() { return this.barrierEl.innerHTML; }
}


export { EntranceGate }