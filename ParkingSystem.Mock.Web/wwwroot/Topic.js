﻿class Topic
{
    static Broadcast = `Broadcast`;

    static Clicked = `Clicked`;
    static Show = `Show`;
    static Open = `Open`;
    static Close = `Close`;
    static Print = `Print`;
    static TicketScanned = `TicketScanned`;
    static PaymentResult = `PaymentResult`;
    static RecognizeLicencePlate = `RecognizeLicencePlate`;
    static LicencePlateResult = `LicencePlateResult`;
}
export { Topic }