﻿using Microsoft.AspNetCore.SignalR;

namespace ParkingSystem.Mock.Web.Hubs
{
    public class MockHub : Hub
    {
        private readonly ILogger<MockHub> _logger;

        public MockHub(ILogger<MockHub> logger)
        {
            _logger = logger;
        }

        public async Task Broadcast(string deviceId, string topic, string payload)
        {
            _logger.LogInformation($"{nameof(Broadcast)} => Device Id: {deviceId}, Topic: {topic}, Payload: {payload}");
            await Clients.Others.SendAsync(nameof(Broadcast), deviceId, topic, payload);
        }
    }
}
