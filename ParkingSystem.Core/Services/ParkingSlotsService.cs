﻿using Marten;
using ParkingSystem.Core.Sagas;

namespace ParkingSystem.Core.Services
{
    public class ParkingSlotsService : IParkingSlotsService
    {
        private readonly IDocumentStore _store;

        public ParkingSlotsService(IDocumentStore store)
        {
            _store = store;
        }

        public bool IsFreeSlotsAvailable()
        {
            using var session = _store.QuerySession();

            var instance = session
                .Query<ParkingSlotsSagaInstance>()
                .FirstOrDefault();

            instance = instance ?? InitSagaInstance();

            return instance.FreeSlotsCount > 0;
        }

        private ParkingSlotsSagaInstance InitSagaInstance()
        {
            var instance = new ParkingSlotsSagaInstance
            {
                CorrelationId = ParkingSlotsSaga.ID,
                FreeSlotsCount = ParkingSlotsSaga.SLOTS_COUNT,
                State = nameof(ParkingSlotsSaga.Idle)
            };

            using var storeSession = _store.LightweightSession();
            storeSession.Store(instance);
            storeSession.SaveChanges();

            return instance;
        }
    }
}
