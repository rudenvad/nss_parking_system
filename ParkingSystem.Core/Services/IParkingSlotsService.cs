﻿namespace ParkingSystem.Core.Services
{
    public interface IParkingSlotsService
    {
        bool IsFreeSlotsAvailable();
    }
}
