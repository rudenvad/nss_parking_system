﻿using HotChocolate.AspNetCore.Authorization;
using Marten;
using ParkingSystem.Core.Sagas;

namespace ParkingSystem.Core.GraphQL
{
    [Authorize]
    public class Query
    {
        private readonly IDocumentStore _store;

        public Query(IDocumentStore store)
        {
            _store = store;
        }

        public IQueryable<TicketSagaInstance> Tickets
        {
            get
            {
                using var session = _store.QuerySession();

                return session
                    .Query<TicketSagaInstance>()
                    .ToList()
                    .AsQueryable();
            }
        }
    }
}
