﻿using MassTransit;
using ParkingSystem.Common.Core;
using ParkingSystem.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Core.Sagas
{
    public class ParkingSlotsSagaInstance : BaseSagaInstance
    {
        public int FreeSlotsCount { get; set; }
    }

    public class ParkingSlotsSaga : MassTransitStateMachine<ParkingSlotsSagaInstance>
    {
        public const int SLOTS_COUNT = 100;
        public static readonly Guid ID = Guid.Parse("66666666-6666-6666-6666-666666666666");

        public State Idle { get; private set; }

        public Event<TicketCreated> TicketCreated { get; private set; }
        public Event<TicketFinalized> TicketFinalized { get; private set; }

        public ParkingSlotsSaga()
        {
            InstanceState(x => x.State);

            Event(() => TicketCreated, e => e.CorrelateById(c => ID));
            Event(() => TicketFinalized, e => e.CorrelateById(c => ID));

            Initially
            (
                When(TicketCreated)
                    .TransitionTo(Idle)
                    .Then(context => context.Saga.FreeSlotsCount = SLOTS_COUNT),
                When(TicketFinalized)
                    .TransitionTo(Idle)
                    .Then(context => context.Saga.FreeSlotsCount = SLOTS_COUNT)
            );

            During(Idle,
                When(TicketCreated)
                    .Then(async context =>
                    {
                        if (context.Saga.FreeSlotsCount < 1)
                            return;

                        context.Saga.FreeSlotsCount--;
                    }),
                When(TicketFinalized)
                    .Then(async context =>
                    {
                        if (context.Saga.FreeSlotsCount >= SLOTS_COUNT)
                            return;

                        context.Saga.FreeSlotsCount++;
                    }));
        }
    }
}
