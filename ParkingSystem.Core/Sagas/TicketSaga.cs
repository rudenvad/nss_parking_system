﻿using MassTransit;
using Microsoft.Extensions.Caching.Distributed;
using ParkingSystem.Common.Core;
using ParkingSystem.Common.Messages;
using ParkingSystem.Common.Opperations;
using ParkingSystem.Core.Messages;
using ParkingSystem.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Core.Sagas
{
    public class TicketSagaInstance : BaseSagaInstance
    {
        public DateTime PaidToDate { get; set; }

        public decimal TotalPaidAmount { get; set; }

        public List<Event> Events { get; set; }

        public TicketSagaInstance()
        {
            Events = new List<Event>();
        }

        public void AddEvent(string name)
        {
            Events.Add(new Event { Name = name, Date = DateTime.UtcNow });
        }

        public class Event
        {
            public string Name { get; set; }

            public DateTime Date { get; set; }
        }
    }

    public class TicketSaga : MassTransitStateMachine<TicketSagaInstance>
    {
        public static IParkingSlotsService ParkingSlotsService { get; set; }
        public static IDistributedCache Cache { get; set; }

        public State WaitingForPayment { get; private set; }
        public State Paid { get; private set; }

        public Event<CreateTicket.Request> CreateTicketRequest { get; private set; }
        public Event<ExitPermission.Request> ExitPermissionRequest { get; private set; }
        public Event<TicketInfo.Request> TicketInfoRequest { get; private set; }
        public Event<TicketPaid> TicketPaid { get; private set; }
        public Event<PaidTimeFinished> PaidTimeFinished { get; private set; }

        public TicketSaga()
        {
            InstanceState(x => x.State);

            Event(() => CreateTicketRequest, e => e.CorrelateById(c => Guid.NewGuid()));
            Event(() => ExitPermissionRequest, e => e.CorrelateById(c => c.Message.TicketId));
            Event(() => TicketInfoRequest, e => e.CorrelateById(c => c.Message.TicketId));
            Event(() => TicketPaid, e => e.CorrelateById(c => c.Message.TicketId));
            Event(() => PaidTimeFinished, e => e.CorrelateById(c => c.Message.TicketId));

            Initially
            (
                When(CreateTicketRequest)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(CreateTicketRequest));

                        if (!ParkingSlotsService.IsFreeSlotsAvailable())
                        {
                            await context.TransitionToState(Final);
                            await context.RespondAsync(new CreateTicket.Response { Error = "No free slots available" });
                            return;
                        }

                        context.Saga.PaidToDate = DateTime.UtcNow;
                        await context.TransitionToState(WaitingForPayment);

                        if (!string.IsNullOrWhiteSpace(context.Message.LicencePlate))
                        {
                            await Cache.SetStringAsync(context.Message.LicencePlate.ToUpper(), context.Saga.CorrelationId.ToString(), new DistributedCacheEntryOptions
                            {
                                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)
                            });
                        }

                        await context.RespondAsync(new CreateTicket.Response { TicketId = context.Saga.CorrelationId });
                        await context.Publish(new TicketCreated());
                    }),
                When(ExitPermissionRequest)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(ExitPermissionRequest));

                        await context.TransitionToState(Final);
                        await context.RespondAsync(new ExitPermission.Response { Error = "Ticket is not valid" });
                    }),
                When(TicketInfoRequest)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(TicketInfoRequest));

                        await context.TransitionToState(Final);
                        await context.RespondAsync(new TicketInfo.Response { Error = "Ticket is not valid" });
                    })
            );

            During(WaitingForPayment,
                When(TicketPaid)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(TicketPaid));

                        context.Saga.TotalPaidAmount += context.Message.Amount;
                        context.Saga.PaidToDate = context.Message.PaidToDate;
                        await context.TransitionToState(Paid);
                        await context.SchedulePublish(context.Message.PaidToDate, new PaidTimeFinished { TicketId = context.Saga.CorrelationId });
                    }));
            During(Paid,
                When(PaidTimeFinished)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(PaidTimeFinished));

                        await context.TransitionToState(WaitingForPayment);
                    }));

            DuringAny
            (
                When(TicketInfoRequest)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(TicketInfoRequest));

                        await context.RespondAsync(new TicketInfo.Response { PaidTo = context.Saga.PaidToDate });
                    }),
                When(ExitPermissionRequest)
                    .Then(async context =>
                    {
                        context.Saga.AddEvent(nameof(ExitPermissionRequest));

                        if (context.Saga.State != nameof(Paid))
                        {
                            await context.RespondAsync(new ExitPermission.Response { Error = "Ticket has not been paid" });
                            return;
                        }
                        
                        await context.TransitionToState(Final);
                        await context.RespondAsync(new ExitPermission.Response { Permitted = true });
                        await context.Publish(new TicketFinalized());
                    })
            );

            During(Final, 
                Ignore(CreateTicketRequest),
                Ignore(ExitPermissionRequest),
                Ignore(TicketInfoRequest),
                Ignore(TicketPaid),
                Ignore(PaidTimeFinished));
        }
    }
}
