using Marten;
using MassTransit;
using Microsoft.Extensions.Caching.Distributed;
using ParkingSystem.Common.Observers;
using ParkingSystem.Common.Settings;
using ParkingSystem.Core.GraphQL;
using ParkingSystem.Core.Sagas;
using ParkingSystem.Core.Services;
using Serilog;
using Serilog.Sinks.Elasticsearch;

var builder = WebApplication.CreateBuilder(args);

var xSection = builder.Configuration.GetSection("X");
var massTransitSettings = xSection.GetSection("MassTransit").Get<MassTransitSettings>();
var redisSettings = xSection.GetSection("Redis").Get<RedisSettings>();
var identityServerSettings = xSection.GetSection("IdentityServer").Get<IdentityServerSettings>();
var elasticsearchSettings = xSection.GetSection("Elasticsearch").Get<ElasticsearchSettings>();

Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .Enrich.WithMachineName()
    .WriteTo.Debug()
    .WriteTo.Console()
    .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticsearchSettings.Uri))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"logs-{nameof(ParkingSystem.Core).ToLower()}"
    })
    .ReadFrom.Configuration(new ConfigurationBuilder()
        .AddJsonFile("appsettings.json")
        .Build())
    .CreateLogger();

builder.Host.UseSerilog();

builder.Services
    .AddAuthentication("Bearer")
    .AddJwtBearer(options =>
    {
        options.Authority = identityServerSettings.Authority;
        options.TokenValidationParameters.ValidateAudience = false;
    });

builder.Services.AddAuthorization();

builder.Services.AddMassTransit(bus =>
{
    bus.AddDelayedMessageScheduler();

    bus.AddSagaStateMachine<TicketSaga, TicketSagaInstance>(x => x.UseTimeout(y => y.Timeout = TimeSpan.FromSeconds(30)))
        .MartenRepository(massTransitSettings.Postgres.ConnectionString);

    bus.AddSagaStateMachine<ParkingSlotsSaga, ParkingSlotsSagaInstance>(x => x.UseTimeout(y => y.Timeout = TimeSpan.FromSeconds(30)))
        .MartenRepository(massTransitSettings.Postgres.ConnectionString);

    bus.UsingRabbitMq((context, cfg) =>
    {
        cfg.UseDelayedMessageScheduler();

        cfg.Host(massTransitSettings.RabbitMQ.Host, "/", host =>
        {
            host.Username(massTransitSettings.RabbitMQ.Username);
            host.Password(massTransitSettings.RabbitMQ.Password);
        });

        cfg.ConfigureEndpoints(context);
    });

    bus.AddConsumeObserver<ConsumeObserver>();
});

builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = redisSettings.ConnectionString;
    options.InstanceName = nameof(ParkingSystem.Core);
});

builder.Services.AddMarten(massTransitSettings.Postgres.ConnectionString);
builder.Services.AddTransient<IParkingSlotsService, ParkingSlotsService>();

builder.Services
    .AddGraphQLServer()
    .AddAuthorization()
    .AddQueryType<Query>()
    .AddType<ObjectType<TicketSagaInstance>>();

builder.Services.AddControllers();

var app = builder.Build();

TicketSaga.ParkingSlotsService = app.Services.GetService<IParkingSlotsService>();
TicketSaga.Cache = app.Services.GetService<IDistributedCache>();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(x => x.MapGraphQL());

app.MapControllers()
    .RequireAuthorization();

app.Run();
