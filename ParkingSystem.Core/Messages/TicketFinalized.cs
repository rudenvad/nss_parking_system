﻿using ParkingSystem.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Core.Messages
{
    public class TicketFinalized : BaseTicketMessage { }
}
