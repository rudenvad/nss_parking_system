﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace ParkingSystem.Core.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LicencePlatesController : ControllerBase
    {
        private readonly IDistributedCache _cache;
        private readonly ILogger<LicencePlatesController> _logger;

        public LicencePlatesController(IDistributedCache cache, ILogger<LicencePlatesController> logger)
        {
            _cache = cache;
            _logger = logger;
        }

        [HttpGet("{licencePlate}")]
        public async Task<LicencePlateInfo> Get(string licencePlate)
        {
            _logger.LogInformation($"Licence Plate Request: ({licencePlate})");

            var ticketId = await _cache.GetStringAsync(licencePlate.ToUpper());

            if (ticketId == null)
                return null;

            return new LicencePlateInfo
            {
                TicketId = ticketId
            };
        }
    }

    public class LicencePlateInfo
    {
        public string TicketId { get; set; }
    }
}
