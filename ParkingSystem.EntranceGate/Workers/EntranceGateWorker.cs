﻿using MassTransit;
using ParkingSystem.Common.Devices;
using ParkingSystem.Common.Opperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.EntranceGate.Workers
{
    public class EntranceGateWorker : BackgroundService
    {
        private readonly IBus _bus;

        private readonly ICamera _camera;
        private readonly IDisplay _display;
        private readonly IPrinter _printer;
        private readonly IBarrier _barrier;

        private TaskCompletionSource ButtonClickedWaiter { get; set; }

        public EntranceGateWorker(IBus bus, IButton button, ICamera camera, IDisplay display, IPrinter printer, IBarrier barrier)
        {
            _bus = bus;

            _camera = camera;
            _display = display;
            _printer = printer;
            _barrier = barrier;

            button.Clicked += async (s, e) => ButtonClickedWaiter?.SetResult();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try { await RunFlow(); }
                catch { }
            }
        }

        private async Task RunFlow()
        {
            await _display.Show("Click button to get ticket");

            ButtonClickedWaiter = new TaskCompletionSource();
            await ButtonClickedWaiter.Task;
            ButtonClickedWaiter = null;

            var licencePlate = await _camera.RecognizeLicencePlate();

            await _display.Show("Requesting entrance...");
            var createTicketResponse = await _bus.Request<CreateTicket.Request, CreateTicket.Response>(new CreateTicket.Request { LicencePlate = licencePlate });

            if (createTicketResponse.Message.Error != null)
            {
                await _display.Show($"Entrance declined: {createTicketResponse.Message.Error}");
                await Task.Delay(3000);
                return;
            }

            await _display.Show("Printing...");
            await Task.Delay(3000);
            await _printer.Print($"{createTicketResponse.Message.TicketId}");
            await _display.Show("Take your ticket and go");

            await _barrier.Open();
            await Task.Delay(5000);
            await _barrier.Close();
        }
    }
}
