using MassTransit;
using ParkingSystem.Common.Devices;
using ParkingSystem.Common.Extensions;
using ParkingSystem.Common.Settings;
using ParkingSystem.EntranceGate.Workers;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices;
using ParkingSystem.Mock.Lib.Settings;
using Serilog;
using Serilog.Sinks.Elasticsearch;

var builder = WebApplication.CreateBuilder(args);

var xSection = builder.Configuration.GetSection("X");
var massTransitSettings = xSection.GetSection("MassTransit").Get<MassTransitSettings>();
var elasticsearchSettings = xSection.GetSection("Elasticsearch").Get<ElasticsearchSettings>();

Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .Enrich.WithMachineName()
    .WriteTo.Debug()
    .WriteTo.Console()
    .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticsearchSettings.Uri))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"logs-{nameof(ParkingSystem.EntranceGate).ToLower()}"
    })
    .ReadFrom.Configuration(new ConfigurationBuilder()
        .AddJsonFile("appsettings.json")
        .Build())
    .CreateLogger();

builder.Host.UseSerilog();

builder.Services.AddMassTransit(bus =>
{
    bus.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host(massTransitSettings.RabbitMQ.Host, "/", host =>
        {
            host.Username(massTransitSettings.RabbitMQ.Username);
            host.Password(massTransitSettings.RabbitMQ.Password);
        });

        cfg.ConfigureEndpoints(context);
    });
});

builder.Services.AddHostedService<EntranceGateWorker>();

builder.Services.Configure<MockSettings>(xSection.GetSection("Mock"));
builder.Services.AddSingleton<MockHubClient>();

var prefix = nameof(ParkingSystem.EntranceGate);
builder.Services.AddSingleton<IButton, ButtonMock>(device => device.Id = $"{prefix}:{nameof(ButtonMock)}");
builder.Services.AddSingleton<ICamera, CameraMock>(device => device.Id = $"{prefix}:{nameof(CameraMock)}");
builder.Services.AddSingleton<IDisplay, DisplayMock>(device => device.Id = $"{prefix}:{nameof(DisplayMock)}");
builder.Services.AddSingleton<IPrinter, PrinterMock>(device => device.Id = $"{prefix}:{nameof(PrinterMock)}");
builder.Services.AddSingleton<IBarrier, BarrierMock>(device => device.Id = $"{prefix}:{nameof(BarrierMock)}");

var app = builder.Build();
app.Run();
