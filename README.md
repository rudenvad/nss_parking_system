# Technology and Language
- .NET 
- C#
> Environment: Visual Studio 2022 

# Relational Database
## Postgres
Application uses postgres database for storing ticket data such as:
- id
- state
- paid total amount
- ticket related events

## PGAdmin
PGAdmin (GUI tool for interaction with postgres) is hosted on: http://rudsoft.tplinkdns.com:7003

Credentials are:
- Email/Username: `rudenvad@rudenvad.com`
- Password: `rudenvad`

# Cache
## Redis
Application uses Redis Distributed Cache to store tickets with recognized licence plate for 24 hours (GDPR).

Case: situation when customer lost ticket, ticket could be found by licence plate.

Cache is used in `ParkingSystem.Core`:
- in `TicketSaga` to store tickets with recognized licence plate
- in `LicencePlatesController` to response on authorized requests with ticket info
> Redis is a distributed in-memory database that allows you to read and write data.
# Messaging
For messaging purposes application uses `Masstransit` with `RabbitMQ`.

It is used by: `ParkingSystem.Core`, `ParkingSystem.EntranceGate`, `ParkingSystem.ExitGate` and `ParkingSystem.PaymentMachine`.

- `TicketSaga` defines ticket lifecycle with `Masstransit Saga` (State Machine).
- `ParkingSystem.EntranceGate`, `ParkingSystem.ExitGate` and `ParkingSystem.PaymentMachine` communicates with `ParkingSystem.Core`
via request->response.

> `TicketSaga` is located in `ParkingSystem.Core`

> MassTransit is free software/open-source .NET-based Enterprise Service Bus software
> RabbitMQ is an open-source message-broker software
# Authorization
## OAuth2
Application uses `OAuth 2.0` for authorizing:
- requests to `LicencePlatesController`
- `Query`

>`LicencePlatesController` is a `ParkingSystem.Core` api controller
>
> How to make authorized requests to `LicencePlatesController` is described in section `REST` in subsection `Postman`

> `Query` is graphql query representation, which is in `ParkingSystem.Core`
> 
> How to use authorized query is described in section `GraphQL` 

## Duende
Authorization is provided by `Duende Identity Server`.

Application uses **free** demo version which provides demo client credentials for **60 minutes** token lifetime.

> demo version details can be discovered here: https://demo.duendesoftware.com
# Interceptors
As interceptor works `ConsumeObserver`, which works with log messages
 
> `ConsumeObserver` is in `ParkingSystem.Common`
# REST
Application uses REST api endpoint `LicencePlatesController` to get tickets by recognized licence plates.

Case: situation when customer lost ticket, ticket could be found by licence plate.

- `LicencePlatesController` requests are authorized with `OAuth 2.0`
- `LicencePlatesController` uses cache for response

>`LicencePlatesController` is a `ParkingSystem.Core` api controller
## Postman
To get ticket by licence plate provide authorized request to `http://rudsoft.tplinkdns.com:7001/api/licencePlates/XXX`,
where `XXX` is a licence plate that was recognised by camera.

1. To make request authorized with **OAuth 2.0** - chose auth type to `OAuth 2.0`.
2. Then change **Configure New Token** settings:
    1. Change **Grant Type** to `Client Credentials`
    2. Fill **Acces token URL** with `https://demo.duendesoftware.com/connect/token`
    3. Fill **Client ID** with: `m2m`
    4. Fill **Client Secret** with: `secret`
    5. Fill **Scope** with: `api`
3. Get new access token and use it.

> Note: because application uses demo version of authorization, token lifetime is only 60 minutes
> (after it should be renewed)

> To test request with existing licence plate, go to http://rudsoft.tplinkdns.com:7000
> and create new ticket with licence plate (fill Camera field with licence plate number & get ticket)

> If no licence plate -> returns status 204 - `No Content`,
> else returns object with ticket id

# GRAPHQL 
Application uses `GraphQL` to get information about tickets (it could be used for generating reports).

- `GraphQL` is added to `ParkingSystem.Core` and `Query` implemented
- `Query` is authorized with `OAuth 2.0`
> `Query` is graphql query representation, which is in `ParkingSystem.Core`
 
## GRAPHQL GUI
GRAPHQL GUI is hosted on: http://rudsoft.tplinkdns.com:7001/graphQL

To test graphql:
1. Open http://rudsoft.tplinkdns.com:7001/graphQL and click `Browse schema`
2. Configure authorization settings:
   1. Open settings: click on **settings wheel**
   2. Set **Type** to `OAuth 2`
   3. Set **Grant Type** to `Client Credentials`
   4. Set **Acces token URL** to `https://demo.duendesoftware.com/connect/token`
   5. Set **Client ID** to `m2m`
   6. Set **Client Secret** to `secret`
   7. Set **Scope** to `api`
   8. Fetch token and apply settings
3. Go to `Operations tab`
4. Write query: `query { tickets { correlationId paidToDate } }`

> Tip for writing queries: `Ctrl`+`Space` will show options to make query
# SignalR
Application uses `SignalR` to provide communication between mock web and backend.
- Used in `ParkingSystem.Mock.Web`
- Used in `ParkingSystem.Mock.Lib`

`SignalR` similarly to `Java Remote Method Invocation` allows web client to invoke methods on server the side.

> SignalR is a free and open-source software library for Microsoft ASP.NET that allows server code to send asynchronous notifications to client-side web applications.
# Deployment
Application is containerized using Docker and deployed as stack on `Rudsoft` **cloud server**. 
- `Rudsoft` server operation system: linux

Containers management could be provided on https://rudsoft.tplinkdns.com/portainer

Credentials:
- Username: `rudenvad`
- Password: `rudenvad`

# Architecture
Application architecture: Event Driven Architecture
> By using Event- Driven architecture, I am separating components to emitters and consumers,
> where emitters don’t even know consumers neither about existence of consumers.
# Deployment initialization
All settings for application is defined in `docker-compose.yml` file.

1. Open solution in Visual Studio 2022
2. Chose `docker-compose` option in `Start up projects`
3. Run application 

OR

Up container with `docker-compose up` command.

# Elasticsearch
`Elasticsearch` is a **search engine**, which is used in application to search information in logs.

Application has a lot of logs, and to find needed ones we can use `Elasticsearch`.
## Kibana
Application uses `Kibana` as UI to work with `Elasticsearch`.

Kibana is hosted on: http://rudsoft.tplinkdns.com:7004

Use example:
1. Open menu and click on `Discover`
2. Change time range `This week`  
3. Write `Broadcast` into search field
4. Click `Refresh`

> Kibana is a proprietary data visualization dashboard software for Elasticsearch
# Design patterns
Used design patterns:
- **StateMachine** (Using masstransit saga)
  - StateMachine definitions are in [ParkingSystem.Core.Sagas](https://gitlab.fel.cvut.cz/rudenvad/nss_parking_system/-/tree/main/ParkingSystem.Core/Sagas)
- **Observer** (C# events)
  - Use example:
  - definition `Broadcast` in `MockClient`, which in `ParkingSystem.Mock.Web`
  - subscription to `Broadcast` in `DeviceMock`, which in `ParkingSystem.Mock.Lib`
- **Builder**:
  - Used in any `Program` class to configure and build application.
- **Dependency injection** (built-in part of framework, using dependency container)
  - Used all over the app.
  - Use example in `ConsumerObserver`, which in `ParkingSystem.Common`, is injected a logger via interface as constructor parameter
- **Singleton**:
  - Use example in any `Program` class from `ParkingSystem.EntranceGate`, `ParkingSystem.ExitGate` or `ParkingSystem.PaymentMachine`
  used for inject mocked devices.
  
I have listed a few built-in patterns, but there more. 
The point is that I know what I am using, and I do not need to 'reinvent the wheel'.  
