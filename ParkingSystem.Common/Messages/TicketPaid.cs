﻿using ParkingSystem.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Messages
{
    public class TicketPaid : BaseTicketMessage 
    {
        public DateTime PaidToDate { get; set; }

        public decimal Amount { get; set;}
    }
}
