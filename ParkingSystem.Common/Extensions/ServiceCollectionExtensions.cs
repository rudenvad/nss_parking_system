﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSingleton<TService, TImplementation>(this IServiceCollection services, Action<TImplementation> configure)
            where TImplementation : TService 
            where TService : class
        {
            return services.AddSingleton<TService>(services =>
            {
                var implementation = ActivatorUtilities.CreateInstance<TImplementation>(services);
                configure(implementation);
                return implementation;
            });
        }
    }
}
