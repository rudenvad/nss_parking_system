﻿using Marten.Schema;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Core
{
    public class BaseSagaInstance : SagaStateMachineInstance
    {
        [Identity]
        public Guid CorrelationId { get; set; }

        public string State { get; set; }
    }
}
