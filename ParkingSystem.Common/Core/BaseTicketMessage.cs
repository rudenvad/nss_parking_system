﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Core
{
    public abstract class BaseTicketMessage
    {
        public Guid TicketId { get; set; }
    }
}
