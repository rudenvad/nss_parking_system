﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Opperations
{
    public class CreateTicket
    {
        public class Request 
        {
            public string LicencePlate { get; set; }
        }

        public class Response
        {
            public Guid TicketId { get; set; }

            public string Error { get; set; }
        }
    }
}
