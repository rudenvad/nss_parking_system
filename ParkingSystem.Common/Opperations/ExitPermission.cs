﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Opperations
{
    public class ExitPermission
    {
        public class Request
        {
            public Guid TicketId { get; set; }
        }

        public class Response
        {
            public bool Permitted { get; set; }

            public string Error { get; set; }
        }
    }
}
