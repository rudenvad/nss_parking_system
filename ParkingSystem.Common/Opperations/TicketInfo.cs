﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Opperations
{
    public class TicketInfo
    {
        public class Request
        {
            public Guid TicketId { get; set; }
        }

        public class Response
        {
            public DateTime PaidTo { get; set; }

            public string Error { get; set; }
        }
    }
}
