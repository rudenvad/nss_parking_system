﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Devices
{
    public interface IPrinter
    {
        Task Print(string text);
    }
}
