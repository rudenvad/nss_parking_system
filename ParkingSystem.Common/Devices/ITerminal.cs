﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Devices
{
    public interface ITerminal
    {
        Task<bool> Pay(decimal amount);
    }
}
