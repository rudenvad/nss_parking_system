﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Devices
{
    public interface IScanner
    {
        event EventHandler<TicketScannedEventArgs> TicketScanned;
    }

    public class TicketScannedEventArgs : EventArgs
    {
        public Guid TicketId { get; set; }

        public string Error { get; set; }
    }
}
