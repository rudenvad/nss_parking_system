﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Common.Settings
{
    public class MassTransitSettings
    {
        public PostgresSettings Postgres { get; set; }

        public RabbitMQSettings RabbitMQ { get; set; }

        public class PostgresSettings
        {
            public string ConnectionString { get; set; }
        }

        public class RabbitMQSettings
        {
            public string Host { get; set; }

            public string Username { get; set; }

            public string Password { get; set; }
        }
    }
}
