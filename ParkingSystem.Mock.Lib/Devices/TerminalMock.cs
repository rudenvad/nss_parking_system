﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class TerminalMock : DeviceMock, ITerminal
    {
        public TerminalMock(MockHubClient mockHubClient) : base(mockHubClient, true) { }

        private TaskCompletionSource<bool> PaymentResultWaiter { get; set; }

        public async Task<bool> Pay(decimal amount)
        {
            PaymentResultWaiter = new TaskCompletionSource<bool>();
            var isSuccess = await PaymentResultWaiter.Task;
            PaymentResultWaiter = null;
            return isSuccess;
        }

        protected override void Handle(string topic, string payload)
        {
            if (topic != "PaymentResult")
                return;

            PaymentResultWaiter?.SetResult(bool.TryParse(payload, out var isSuccess) && isSuccess);
        }
    }
}
