﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class ButtonMock : DeviceMock, IButton
    {
        public ButtonMock(MockHubClient mockHubClient) : base(mockHubClient, true) { }

        protected override void Handle(string topic, string payload)
        {
            if (topic == nameof(Clicked))
            {
                Clicked?.Invoke(this, null);
            }
        }

        public event EventHandler Clicked;
    }
}
