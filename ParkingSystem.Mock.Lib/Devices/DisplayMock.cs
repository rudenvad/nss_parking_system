﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class DisplayMock : DeviceMock, IDisplay
    {
        public DisplayMock(MockHubClient mockHubClient) : base(mockHubClient) { }

        public async Task Show(string text)
        {
            await Broadcast(nameof(Show), text);
        }
    }
}
