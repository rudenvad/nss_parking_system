﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class CameraMock : DeviceMock, ICamera
    {
        public CameraMock(MockHubClient mockHubClient) : base(mockHubClient, true) { }

        private TaskCompletionSource<string> LicencePlateResultWaiter { get; set; }

        public async Task<string> RecognizeLicencePlate()
        {
            await Broadcast(nameof(RecognizeLicencePlate));

            LicencePlateResultWaiter = new TaskCompletionSource<string>();
            var licencePlate = await LicencePlateResultWaiter.Task;
            LicencePlateResultWaiter = null;
            return licencePlate;
        }

        protected override void Handle(string topic, string payload)
        {
            if (topic != "LicencePlateResult")
                return;

            LicencePlateResultWaiter?.SetResult(payload);
        }
    }
}
