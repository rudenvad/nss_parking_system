﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class ScannerMock : DeviceMock, IScanner
    {
        public ScannerMock(MockHubClient mockHubClient) : base(mockHubClient, true) { }

        protected override void Handle(string topic, string payload)
        {
            if (topic != nameof(TicketScanned))
                return;

            if (!Guid.TryParse(payload, out var ticketId))
            {
                TicketScanned?.Invoke(this, new TicketScannedEventArgs { Error = "Invalid ticket" });
                return;
            }

            TicketScanned?.Invoke(this, new TicketScannedEventArgs { TicketId = ticketId });
        }

        public event EventHandler<TicketScannedEventArgs> TicketScanned;
    }
}
