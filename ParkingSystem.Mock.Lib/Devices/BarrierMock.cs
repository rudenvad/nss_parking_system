﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class BarrierMock : DeviceMock, IBarrier
    {
        public BarrierMock(MockHubClient mockHubClient) : base(mockHubClient) { }

        public async Task Close()
        {
            await Broadcast(nameof(Close));
            await Task.Delay(1000);
        }

        public async Task Open()
        {
            await Broadcast(nameof(Open));
            await Task.Delay(1000);
        }
    }
}
