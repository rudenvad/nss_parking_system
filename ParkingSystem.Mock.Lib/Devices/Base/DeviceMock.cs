﻿using ParkingSystem.Mock.Lib.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices.Base
{
    public abstract class DeviceMock
    {
        public string Id { get; set; }

        private readonly MockHubClient _mockHubClient;

        public DeviceMock(MockHubClient mockHubClient, bool subscribeToHub = false)
        {
            _mockHubClient = mockHubClient;

            if (subscribeToHub)
            {
                mockHubClient.Broadcasted += (s, e) =>
                {
                    if (e.DeviceId != Id)
                        return;

                    Handle(e.Topic, e.Payload);
                }; 
            }
        }

        protected virtual void Handle(string topic, string payload) { }

        protected async Task Broadcast(string topic, string payload = null)
        {
            await _mockHubClient.Broadcast(Id, topic, payload);
        }
    }
}
