﻿using ParkingSystem.Common.Devices;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Devices
{
    public class PrinterMock : DeviceMock, IPrinter
    {
        public PrinterMock(MockHubClient mockHubClient) : base(mockHubClient) { }

        public async Task Print(string text)
        {
            await Broadcast(nameof(Print), text);
            await Task.Delay(1000);
        }
    }
}
