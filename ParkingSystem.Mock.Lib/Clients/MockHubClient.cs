﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Options;
using ParkingSystem.Mock.Lib.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingSystem.Mock.Lib.Clients
{
    public class MockHubClient
    {
        private HubConnection Connection { get; set; }

        public MockHubClient(IOptions<MockSettings> options)
        {
            Connection = new HubConnectionBuilder()
                .WithUrl(options.Value.HubUrl)
                .Build();

            Connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await Connection.StartAsync();
            };

            Connection.On<string, string, string>(nameof(Broadcast), (deviceId, topic, payload) =>
            {
                Broadcasted?.Invoke(this, new BroadcastedEventArgs { DeviceId = deviceId, Topic = topic, Payload = payload });
            });

            Connection.StartAsync().Wait();
        }

        public async Task Broadcast(string deviceId, string topic, string payload)
        {
            await Connection.SendAsync(nameof(Broadcast), deviceId, topic, payload);
        }

        public event EventHandler<BroadcastedEventArgs> Broadcasted;
    }

    public class BroadcastedEventArgs : EventArgs
    {
        public string DeviceId { get; set; }

        public string Topic { get; set; }

        public string Payload { get; set; }
    }
}
