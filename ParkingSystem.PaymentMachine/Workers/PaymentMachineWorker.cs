using MassTransit;
using ParkingSystem.Common.Devices;
using ParkingSystem.Common.Messages;
using ParkingSystem.Common.Opperations;

namespace ParkingSystem.PaymentMachine.Workers
{
    public class PaymentMachineWorker : BackgroundService
    {
        private readonly IBus _bus;

        private readonly IDisplay _display;
        private readonly ITerminal _terminal;

        private TaskCompletionSource<TicketScannedEventArgs> TicketScannedWaiter { get; set; }

        public PaymentMachineWorker(IBus bus, IDisplay display, IScanner scanner, ITerminal terminal)
        {
            _bus = bus;
            _display = display;
            _terminal = terminal;

            scanner.TicketScanned += (s, e) =>
            {
                TicketScannedWaiter?.SetResult(e);
            };
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try { await RunFlow(); }
                catch { }
            }
        }

        private async Task RunFlow()
        {
            await _display.Show("Scan your ticket");

            TicketScannedWaiter = new TaskCompletionSource<TicketScannedEventArgs>();
            var ticketScannedArgs = await TicketScannedWaiter.Task;
            TicketScannedWaiter = null;

            if (ticketScannedArgs.Error != null)
            {
                await _display.Show(ticketScannedArgs.Error);
                await Task.Delay(3000);
                return;
            }

            await _display.Show("Requesting ticket info...");

            var ticketInfoResponse = await _bus.Request<TicketInfo.Request, TicketInfo.Response>(new TicketInfo.Request
            {
                TicketId = ticketScannedArgs.TicketId
            });

            if (ticketInfoResponse.Message.Error != null)
            {
                await _display.Show(ticketInfoResponse.Message.Error);
                await Task.Delay(3000);
                return;
            }

            var paidTo = ticketInfoResponse.Message.PaidTo;

            if (paidTo > DateTime.UtcNow)
            {
                await _display.Show($"Ticket already paid until: {paidTo.ToLocalTime()}");
                await Task.Delay(3000);
                return;
            }

            var amount = (DateTime.UtcNow - paidTo).Minutes;

            if (amount == 0)
                amount = 1;

            await _display.Show($"Please pay {amount} UAH");

            if (!await _terminal.Pay(amount))
            {
                await _display.Show($"Payment failed");
                await Task.Delay(3000);
                return;
            }

            paidTo = DateTime.UtcNow.AddMinutes(5);

            await _bus.Publish(new TicketPaid
            {
                TicketId = ticketScannedArgs.TicketId,
                PaidToDate = paidTo,
                Amount = amount
            });

            await _display.Show($"Ticket paid until: {paidTo}");
            await Task.Delay(3000);
        }
    }
}