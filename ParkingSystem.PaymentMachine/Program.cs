using MassTransit;
using ParkingSystem.Common.Devices;
using ParkingSystem.Common.Extensions;
using ParkingSystem.Common.Settings;
using ParkingSystem.Mock.Lib.Clients;
using ParkingSystem.Mock.Lib.Devices;
using ParkingSystem.Mock.Lib.Settings;
using ParkingSystem.PaymentMachine.Workers;
using Serilog;
using Serilog.Sinks.Elasticsearch;

var builder = WebApplication.CreateBuilder(args);

var xSection = builder.Configuration.GetSection("X");
var massTransitSettings = xSection.GetSection("MassTransit").Get<MassTransitSettings>();
var elasticsearchSettings = xSection.GetSection("Elasticsearch").Get<ElasticsearchSettings>();

Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .Enrich.WithMachineName()
    .WriteTo.Debug()
    .WriteTo.Console()
    .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticsearchSettings.Uri))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"logs-{nameof(ParkingSystem.PaymentMachine).ToLower()}"
    })
    .ReadFrom.Configuration(new ConfigurationBuilder()
        .AddJsonFile("appsettings.json")
        .Build())
    .CreateLogger();

builder.Host.UseSerilog();

builder.Services.AddMassTransit(bus =>
{
    bus.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host(massTransitSettings.RabbitMQ.Host, "/", host =>
        {
            host.Username(massTransitSettings.RabbitMQ.Username);
            host.Password(massTransitSettings.RabbitMQ.Password);
        });

        cfg.ConfigureEndpoints(context);
    });
});

builder.Services.AddHostedService<PaymentMachineWorker>();

builder.Services.Configure<MockSettings>(xSection.GetSection("Mock"));
builder.Services.AddSingleton<MockHubClient>();

var prefix = nameof(ParkingSystem.PaymentMachine);
builder.Services.AddSingleton<IScanner, ScannerMock>(device => device.Id = $"{prefix}:{nameof(ScannerMock)}");
builder.Services.AddSingleton<IDisplay, DisplayMock>(device => device.Id = $"{prefix}:{nameof(DisplayMock)}");
builder.Services.AddSingleton<ITerminal, TerminalMock>(device => device.Id = $"{prefix}:{nameof(TerminalMock)}");

var app = builder.Build();
app.Run();
